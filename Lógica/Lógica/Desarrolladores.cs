﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    public class Desarrolladores:Empleados
    {
        // CORRECCIÓN: SE DEFINIÓ UN ENUM PERO NO SE CREO UNA PROPIEDAD DE ESTE TIPO DE ENUM
        public enum Experiencia { P,B,M}
        public Lideres Lider { get; set; }

        //CORRECCIÓN: ESTO DEBÍA USAR PARA LA PRIMERA PARTE EL MÉTODO BASE A REALIZAR EN LA CLASE EMPLEADOS
        public virtual string DetalleEmpleado()
        {
            return Nombre + Apellido + DNI + Lider.Nombre;
                
        }

        //CORRECCIÓN: CÓDIGO INCOMPLETO
        public override void ValidarTickets(TICKET.Tipo ticketstipo)
        {
            if (ticketstipo==TICKET.Tipo.accesobasededatos)
            {
                if (Lideres.AreasaCargo==Lideres.AreasaCargo.Desarrollo)
                {

                }
            }
        }
    }
}
