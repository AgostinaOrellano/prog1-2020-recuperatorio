﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
   public class TICKET
    {
        public int autonumerico { get; set; }

        //CORRECCIÓN: LOS ENUMERADORES DEBEN ESTAR JUNTOS Y APARTE DE LAS PROPIEDADES PARA TENER UN CÓDIGO LIMPIO.
        // ADEMAS SE DEFINIÓ UN ENUM PERO NO SE CREO UNA PROPIEDAD DE ESTE TIPO DE ENUM
        public enum Tipo
        {
            gestionpermisos=1,
            accesobasededatos=2,
            altausuario=3
        }
        public DateTime Fechacreacion { get; set; }
        //CORRECCIÓN: LOS ENUMERADORES DEBEN ESTAR JUNTOS Y APARTE DE LAS PROPIEDADES PARA TENER UN CÓDIGO LIMPIO.
        // ADEMAS SE DEFINIÓ UN ENUM PERO NO SE CREO UNA PROPIEDAD DE ESTE TIPO DE ENUM
        public enum Estadoticket
        {
            Abierto=1,
            Enproceso=2,
            Resuelto=3,
        }
        public string Observacionusuario { get; set; }
        public string Comentarioresolucion { get; set; }

        //CORRECCIÓN: INCORRECTO, ESTO SIEMPRE VA A DEVOLVER 1.
        public int CodigoNumerico() 
        {
            int cod = 0;
            cod ++;
            return cod;
        }
    }
}
