﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    abstract public class Empleados
    {
        public int DNI {get;set;}
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Area { get; set; }
        public DateTime Fechanacimiento { get; set; }
        public string Localidad { get; set; }
        //CORRECCIÓN: LOS ENUMERADORES DEBEN ESTAR JUNTOS Y APARTE DE LAS PROPIEDADES PARA TENER UN CÓDIGO LIMPIO.
        // ADEMAS SE DEFINIÓ UN ENUM PERO NO SE CREO UNA PROPIEDAD DE ESTE TIPO DE ENUM
        public enum TipoEmpleado
        {
            Desarrolladoress= 1,
            Lideress= 2,
        }

        //CORRECCIÓN: FALTA EL MÉTODO BASE PARA SOLVER LA PRIMER PARTE DE LA DESCRIPCIÓN

        public abstract void ValidarTickets(TICKET.Tipo ticketstipo);




    }
}
